import os
import csv
from fstats_1vs1 import *

total_betted_money = 0
total_won_money = 0
succ_bets = 0
total_bets = 0
BET_VALUE = 100


def list_training_files():
    path = os.getcwd()+"/static"
    list_of_files = []

    for filename in os.listdir(path):
        list_of_files.append(filename)
    
    return list_of_files


def read_file(file):
    lines = []
    with open(file, 'r' ) as f:
        reader = csv.DictReader(f)
        lines = list(reader)
    return lines


def predict_result(line, model):
    PA_win_prob = ExtendedStats.win_probability(
                                                 [model.players[line['HomeTeam']].skill],
                                                 [model.players[line['AwayTeam']].skill]
                                               )
    PB_win_prob = 1 - PA_win_prob
    D_prob = trueskill.quality_1vs1(model.players[line['HomeTeam']].skill, model.players[line['AwayTeam']].skill)-0.12
    
    prediction = 0
    if PA_win_prob == max([PA_win_prob, D_prob, PB_win_prob]):
        prediction = 1
    elif PB_win_prob == max([PA_win_prob, D_prob, PB_win_prob]):
        prediction = 2
    
    # print(PA_win_prob, D_prob, PB_win_prob, '=>', prediction)
    return prediction


def place_bet():
    global total_betted_money
    global total_bets
    total_betted_money += BET_VALUE
    total_bets += 1
    
    return True


def evaluate_bet(line, model):
    # print('Bet')
    global total_won_money
    global succ_bets
    # if total_won_money > total_betted_money * 1.5:
        # return
    prediction = predict_result(line, model)
    
    betted = False
    # if prediction == 0 and (float(line['B365H']) > float(line['B365D']) or float(line['B365A']) > float(line['B365D'])):
    if prediction == 0 and (model.players[line['HomeTeam']].draw_probability() > 0.33 or model.players[line['AwayTeam']].draw_probability() > 0.33):
    # if prediction == 0:
        betted = place_bet()
    
    actual_result = 0
    if line['FTR'] == 'H':
        actual_result = 1
    elif line['FTR'] == 'A':
        actual_result = 2
    
    # if actual_result == 0:
        # print(line['B365H'], line['B365D'], line['B365A'])
        # print(model.players[line['HomeTeam']].draw_probability(), model.players[line['AwayTeam']].draw_probability())
        # print(line['HomeTeam'], line['AwayTeam'], prediction, actual_result, betted)
        # print()
    
    if betted and prediction == actual_result:
        succ_bets += 1
        if prediction == 1:
            total_won_money += BET_VALUE * float(line['B365H'])
        if prediction == 2:
            total_won_money += BET_VALUE * float(line['B365A'])
        if prediction == 0:
            total_won_money += BET_VALUE * float(line['B365D'])
    # print(total_betted_money, ';', total_won_money, ';', total_won_money - total_betted_money )


def benchmark(training_file, betting_file, file_format=InputFileReader.Format.SS_CSV, train_set_size=0):
    global total_betted_money
    global total_won_money
    global succ_bets
    global total_bets
    
    total_betted_money = 0
    total_won_money = 0
    succ_bets = 0
    total_bets = 0
    
    ex_stats = ExtendedStats()
    if file_format == InputFileReader.Format.SS_CSV:
        training_data = InputFileReader.fromSkySportsCSV(training_file)
    else:
        training_data = InputFileReader.fromFootballDataCSV(training_file)
    if train_set_size > 0:
        training_data = training_data[:train_set_size]
    ex_stats.set_training_data(training_data)
    
    betting_matches = read_file(betting_file)
    if train_set_size > 0:
        betting_matches = betting_matches[train_set_size:]
    
    for line in betting_matches:
        # print(line['HomeTeam'], line['FTHG'], line['FTAG'], line['AwayTeam'])
        evaluate_bet(line, ex_stats)
        # update the model with the latest result
        ex_stats.train( InputLine(line['HomeTeam'], line['FTHG'], line['FTAG'], line['AwayTeam']) )
    
    print('League Draw probability', ex_stats.draw_probability)
    print('Invested', total_betted_money)
    print('Returned', total_won_money)
    print('Profit', total_won_money - total_betted_money, end='')
    if total_betted_money:
        print(' => ROI', (total_won_money-total_betted_money)/total_betted_money*100, "%")
        print('Bets', succ_bets, '/', total_bets, '=>', succ_bets/total_bets)
    print()


if __name__ == "__main__":
    print('Ligue 1 2019/2020')
    benchmark("benchmark/2019_2020_ligue1.csv", "benchmark/2019_2020_ligue1.csv", file_format=InputFileReader.Format.FD_CSV, train_set_size=200)
    print('Serie A 2019/2020')
    benchmark("benchmark/2019_2020_seriea.csv", "benchmark/2019_2020_seriea.csv", file_format=InputFileReader.Format.FD_CSV, train_set_size=200)
    print('PremierLeague 2019/2020')
    benchmark("benchmark/2019_2020_premier_league.csv", "benchmark/2019_2020_premier_league.csv", file_format=InputFileReader.Format.FD_CSV, train_set_size=200)
    
    print('Ligue 1 2018/2019')
    benchmark("benchmark/2018_2019_ligue1.csv", "benchmark/2018_2019_ligue1.csv", file_format=InputFileReader.Format.FD_CSV, train_set_size=200)
    print('Serie A 2018/2019')
    benchmark("benchmark/2018_2019_seriea.csv", "benchmark/2018_2019_seriea.csv", file_format=InputFileReader.Format.FD_CSV, train_set_size=200)
    print('PremierLeague 2018/2019')
    benchmark("benchmark/2018_2019_premier_league.csv", "benchmark/2018_2019_premier_league.csv", file_format=InputFileReader.Format.FD_CSV, train_set_size=200)
    
    print('Ligue 1 2017/2018')
    benchmark("benchmark/2017_2018_ligue1.csv", "benchmark/2017_2018_ligue1.csv", file_format=InputFileReader.Format.FD_CSV, train_set_size=200)
    print('Serie A 2017/2018')
    benchmark("benchmark/2017_2018_seriea.csv", "benchmark/2017_2018_seriea.csv", file_format=InputFileReader.Format.FD_CSV, train_set_size=200)
    print('PremierLeague 2017/2018')
    benchmark("benchmark/2017_2018_premier_league.csv", "benchmark/2017_2018_premier_league.csv", file_format=InputFileReader.Format.FD_CSV, train_set_size=200)
