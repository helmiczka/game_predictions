import sys
import re
from trueskill import TrueSkill, Rating, rate, BETA
import trueskill
import itertools
import math
from enum import Enum
import csv


# env = TrueSkill(draw_probability=0.24)
# env.make_as_global()

G_DRAWS = 0

class Player:

    def __init__(self):
        self.games = 1
        self.skill = Rating()
        self.draws = 0

    def rank(self):
        return self.skill.mu - 3*self.skill.sigma

    def draw_probability(self):
        return float(self.draws/self.games)
    

class InputLine:

    def __init__(self, A, GA, GB, B):
        self.A = A
        self.GA = GA
        self.B = B
        self.GB = GB


class InputFileReader:
    
    class Format(Enum):
        SS_CSV = 1
        FD_CSV = 2

    def line_contains_match_result(line):
        # pattern = re.compile("^\D+,\D+,\d+,\d+,\D+,\D+")
        pattern = re.compile("^(\D+) (\d+) (\d+) (\D+) FT")
        return pattern.match(line)


    def parse_SS_CSV_file(all_lines):
        games = []
        # get players names
        for line in all_lines:
            pattern_match = InputFileReader.line_contains_match_result(line)
            if pattern_match:
                A, GA, GB, B = pattern_match.groups()[0:4]
                GA = int(GA)
                GB = int(GB)
                games.append(InputLine(A, GA, GB, B))
        
        # SkySports CSV is order from the most recent games by default
        return list(reversed(games))


    def parse_FD_CSV_file(all_lines):
        games = []
        reader = csv.DictReader(all_lines)
        lines = list(reader)
        for line in lines:
            games.append(InputLine(line['HomeTeam'], line['FTHG'], line['FTAG'], line['AwayTeam']))
        return games
        

    def fromSkySportsCSV(file):
        f = open(file, mode="r", encoding="utf-8")
        return InputFileReader.parse_SS_CSV_file(f.readlines())
    

    def fromFootballDataCSV(file):
        games = []
        with open(file, 'r' ) as f:
            reader = csv.DictReader(f)
            lines = list(reader)
        for line in lines:
            games.append(InputLine(line['HomeTeam'], line['FTHG'], line['FTAG'], line['AwayTeam']))
        return games


class Match:

    def __init__(self, input_line, players):
        self.PA1 = input_line.A
        self.GA = int(input_line.GA)
        self.GB = int(input_line.GB)
        self.PB1 = input_line.B
        for player in [self.PA1, self.PB1]:
            if player in players:
                players[player].games += 1
            else:
                players[player] = Player()
            
    
    def recalculate_skills(self, players):
        
        if self.GB > self.GA:
            new_PB1_rating, new_PA1_rating = trueskill.rate_1vs1(players[self.PB1].skill, players[self.PA1].skill)
        elif self.GB == self.GA:
            new_PA1_rating, new_PB1_rating = trueskill.rate_1vs1(players[self.PA1].skill, players[self.PB1].skill, drawn=True)
            global G_DRAWS
            G_DRAWS += 1
            players[self.PA1].draws += 1
            players[self.PB1].draws += 1
        else:
            new_PA1_rating, new_PB1_rating = trueskill.rate_1vs1(players[self.PA1].skill, players[self.PB1].skill)
        
        players[self.PA1].skill = new_PA1_rating
        players[self.PB1].skill = new_PB1_rating


class ExtendedStats():

    def __init__(self):
        self.players = {}
        self.players_history = {}
        self.match_history = []
        self.total_match_count = 0


    def set_training_data(self, lines):
        self.file_lines = lines
        self.parse_file()

    
    def train(self, input_line):
        match = Match(input_line, self.players)
        match.recalculate_skills(self.players)
        self.make_skills_snapshot(match)
        self.match_history.append(match)


    def parse_file(self):
        draws = 0
        homes = 0
        aways = 0
        # get all team names in the first loop
        for line in self.file_lines:
            self.add_names(line)
            if line.GA == line.GB:
                draws +=1
            elif line.GA > line.GB:
                homes += 1
            elif line.GA < line.GB:
                aways += 1
        # print('H  D  A')
        # print(homes,draws,aways)
        self.draw_probability = float(draws/len(self.file_lines))
        # print(draw_probability)
        env = TrueSkill(draw_probability=self.draw_probability)
        env.make_as_global()

        for line in self.file_lines:
            self.total_match_count += 1
            self.train(line)
    
    def add_names(self, input_line):
        if not input_line.A in self.players_history:
            self.players_history[input_line.A] = [0]
        if not input_line.B in self.players_history:
            self.players_history[input_line.B] = [0]


    def make_skills_snapshot(self, match):
        for p in self.players_history:
            # duplicate last record
            self.players_history[p].append(self.players_history[p][-1])
            # if the player played the match, update it with new value
        self.players_history[match.PA1][-1] = self.players[match.PA1].rank()
        self.players_history[match.PB1][-1] = self.players[match.PB1].rank()
    
    
    def win_probability(team1, team2):
        delta_mu = sum(r.mu for r in team1) - sum(r.mu for r in team2)
        sum_sigma = sum(r.sigma ** 2 for r in itertools.chain(team1, team2))
        size = len(team1) + len(team2)
        denom = math.sqrt(size * (BETA * BETA) + sum_sigma)
        ts = trueskill.global_env()
        return ts.cdf(delta_mu / denom)
    
    
    def real_results(self, team1, team2):
        matches = []
        for match in self.match_history:
            if (((match.PA1 in team1) and (match.PB1 in team2))
                or
                ((match.PA1 in team2) and (match.PB1 in team1))):
                matches.append(match)
            
        return matches
    
    
    def print_history(self):
        for p in self.players_history:
            print(p,',',end='')
        print()
        
        # print to screen
        for i in range(0, self.total_match_count+1):
            for p in self.players_history:
                print(round(self.players_history[p][i], 1),',',end='')
            print()
        
        # print to file
        with open('skill_history.csv', mode="w", encoding="utf-8") as fw:
            for p in self.players_history:
                print(p,',',end='', file=fw)
            print(file=fw)
            
            for i in range(0, self.total_match_count+1):
                for p in self.players_history:
                    print(self.players_history[p][i],',',end='', file=fw)
                print(file=fw)


if __name__ == "__main__":
    stats = ExtendedStats()
    training_data = InputFileReader.fromFootballDataCSV(sys.argv[1])
    stats.set_training_data(training_data)

    # print('\nName \t games \t skill\n')
    # for player, info in players.items():
        # print(player, '\t', info.games, '\t', info.skill)

    print('\nSorted by TrueSkill rank\nName \t games \t rank\n')
    for player, info in sorted(stats.players.items(), key = lambda name: stats.players[name[0]].rank(), reverse=True):
        print(player, '\t', info.games, '\t', round(info.rank(), 1), info.skill)

    stats.print_history()
    
    def predict(A, B):
        print(A, ':', B)
        print(ExtendedStats.win_probability(
                              [stats.players[A].skill],
                              [stats.players[B].skill]),
                              'that', A, 'wins')

        print(trueskill.quality_1vs1(stats.players[A].skill, stats.players[B].skill)-0.11, 'chance to draw')

        for match in stats.real_results([A], [B]):
            print(match.PA1, match.GA, match.GB, match.PB1)
        print(A, 'draw probability', stats.players[A].draw_probability())
        print(B, 'draw probability', stats.players[B].draw_probability())
        print()
    
    print('Total games', len(stats.match_history))
    print('Draws', G_DRAWS)
    predict('Brest', 'Angers')
    predict('Rennes', 'Nimes')
    predict('Paris SG', 'Bordeaux')
    # predict('Nantes', 'Metz')
    # predict('Nimes', 'Angers')
    # predict('Toulouse', 'Nice')
    # predict('Lyon', 'Strasbourg')
    # predict('Brest', 'St Etienne')
    # predict('Reims', '')
    # predict('Lille', 'Marseille')
