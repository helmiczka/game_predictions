from flask import Flask, render_template, request
from flask import current_app
import os
from fstats_1vs1 import *

app = Flask(__name__)

def list_files():
    path = os.getcwd()+"/benchmark"
    list_of_files = []

    for filename in os.listdir(path):
        list_of_files.append(filename)
    
    return list_of_files

def stats_table(ex_stats):
    table = []
    table_header = ['Name','Games', 'Rank', 'Mean value', 'Standard deviation']
    table.append(table_header)

    for player, info in sorted(ex_stats.players.items(), key = lambda name: ex_stats.players[name[0]].rank(), reverse=True):
       table.append([player, str(info.games),
                    str(round(info.rank(), 1)),
                    str(round(info.skill.mu, 2)),
                    str(round(info.skill.sigma, 2))])
    
    return table

@app.route('/', methods=['GET', 'POST'])
def hello():
    if request.method == 'POST':
        season = request.form['season']
        file_lines = ""
        with current_app.open_resource('benchmark/'+season, mode="rb") as f:
            file_lines = f.read().decode('utf-8').split('\n')
        
        ex_stats = ExtendedStats()
        ex_stats.set_training_data(InputFileReader.parse_FD_CSV_file(file_lines))
        
        win_probability = -1
        # sel_PA1, sel_PB1, sel_PA2, sel_PB2 = list(ex_stats.players.keys())[0:4]
        if 'PA1' in request.form and request.form['PA1'] in ex_stats.players:
            PA1 = ex_stats.players[request.form['PA1']]
            PB1 = ex_stats.players[request.form['PB1']]
            win_probability = ExtendedStats.win_probability(
                          [PA1.skill],
                          [PB1.skill]
                          )
            draw_probability = trueskill.quality_1vs1(PA1.skill, PB1.skill)
            return render_template(
                               'page.html',
                               stats=stats_table(ex_stats),
                               seasons=list_files(),
                               matches=ex_stats.match_history,
                               players = ex_stats.players,
                               selected=season,
                               win_probability=win_probability,
                               draw_probability=draw_probability,
                               past_matches=ex_stats.real_results(
                                                                  [request.form['PA1']],
                                                                  [request.form['PB1']]
                                                                 ),
                               sel_PA1=request.form['PA1'],
                               sel_PB1=request.form['PB1'])
        
        return render_template(
                               'page.html',
                               stats=stats_table(ex_stats),
                               seasons=list_files(),
                               matches=ex_stats.match_history,
                               players = ex_stats.players,
                               selected=season)

    return render_template('page.html', seasons=list_files())

if __name__ == "__main__":
    app.debug = True
    app.run()

